const chalk = require('chalk')
const fs = require('fs')

const getNotes = () => {
    const notes = loadNotes()
    console.log(chalk.inverse('Your notes'))
    notes.forEach(note => {
        console.log(note.title)
    })
}

const addNote = (title, body) => {
    const notes = loadNotes()
    const duplicateNote = notes.find(n => n.title === title)

    console.log(duplicateNote)

    if(!duplicateNote) {
        notes.push({
            title,
            body
        })
        saveNotes(notes)
        console.log(chalk.green.inverse('New notes added!'))
    } else {
        console.log('Note title taken!')
    }
}

const removeNote = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter(n => n.title !== title)
    if(notes.length > notesToKeep.length) {
        console.log(chalk.green.inverse('Note removed!'))
        saveNotes(notesToKeep)
    } else {
        console.log(chalk.red.inverse('No note found'))
    }
}

const getNote = (title) => {
    const notes = loadNotes()
    const note = notes.find(n => n.title === title)
    if(note) {
        console.log(chalk.inverse(note.title) + ' ' + note.body)
    } else {
        console.log(chalk.red.inverse("Cannot find note") )
    }
}

const saveNotes = (notes) => {
    fs.writeFileSync('notes.json', JSON.stringify(notes))
}

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        return JSON.parse(dataBuffer.toString())
    } catch (e) {
        return []
    }
}

module.exports = {
    getNotes,
    addNote,
    removeNote,
    getNote
}